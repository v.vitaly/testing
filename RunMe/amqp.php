<?php

use App\SingleAmqpConnection;
use PhpAmqpLib\Connection\AMQPLazyConnection;
use PhpAmqpLib\Connection\AMQPStreamConnection;

require_once __DIR__ . '/../vendor/autoload.php';

$conn = new AMQPStreamConnection('127.0.0.1', 5673, 'guest', 'guest');
SingleAmqpConnection::getInstance()->initialize($conn);

var_dump(SingleAmqpConnection::getInstance() === SingleAmqpConnection::getInstance());
var_dump(SingleAmqpConnection::getInstance()->getConnection() === SingleAmqpConnection::getInstance()->getConnection());

$lazyConn = new AMQPLazyConnection('127.0.0.1', 5673, 'guest', 'guest');

SingleAmqpConnection::getInstance()->initialize($lazyConn);
var_dump(SingleAmqpConnection::getInstance() === SingleAmqpConnection::getInstance());
var_dump(SingleAmqpConnection::getInstance()->getConnection() === SingleAmqpConnection::getInstance()->getConnection());
var_dump(SingleAmqpConnection::getInstance()->getConnection() === $lazyConn);