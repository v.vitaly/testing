<?php

$urls = [
  'http://httpbin.org/delay/3?test=1', // delay in 3 seconds
  'http://httpbin.org/delay/5?test=2', // delay in 5 seconds
  'http://httpbin.org/delay/1?test=3', // delay in 1 second
];

$channels = [];

$master = curl_multi_init();

// initialize
foreach ($urls as $url) {
    $ch = curl_init();
    curl_setopt_array($ch, [
      CURLOPT_URL => $url,
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_HEADER => false,
    ]);

    curl_multi_add_handle($master, $ch);

    $channels[$url] = $ch;
}

$startTime = microtime(true);

// execute

$active = null;
do {
    $mrc = curl_multi_exec($master, $active);
} while ($mrc === CURLM_CALL_MULTI_PERFORM);

while ($active && $mrc === CURLM_OK) {
    curl_multi_select($master);

    do {
        $mrc = curl_multi_exec($master, $active);
    } while ($mrc === CURLM_CALL_MULTI_PERFORM);

    while ($info = curl_multi_info_read($master)) {
        echo curl_multi_getcontent($info['handle']);
    }
}

$endTime = microtime(true);

echo 'Elapsed time is ' . ($endTime - $startTime) . ' sec' . PHP_EOL;

// finalize
foreach ($channels as $url => $channel) {
    curl_multi_remove_handle($master, $channel);
}

curl_multi_close($master);