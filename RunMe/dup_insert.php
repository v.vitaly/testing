<?php

use App\TestingModel;
use yii\console\widgets\Table;
use yii\db\Connection;
use yii\db\Query;

require_once __DIR__ . '/../vendor/autoload.php';

define('YII_DEBUG', true);

$config = [
    'id' => 'test-meta-tags',
    'basePath' => __DIR__ . '/../src',
    'components' => [
        'db' => [
            'class' => Connection::class,
            'dsn' => 'mysql:host=127.0.0.1;port=33306;dbname=testdb',
            'username' => 'root',
            'password' => 'root',
            'charset' => 'utf8',
            'enableSchemaCache' => false,
        ],
    ],
];
$app = new yii\console\Application($config);

$sql = <<<SQL
create table if not exists testing
(
    id int unsigned not null auto_increment,
    uniq_key varchar(16) not null,
    normal_col varchar(16) not null,
    primary key (id),
    unique key uniq (uniq_key)
) engine=InnoDb default charset=utf8 collate=utf8_unicode_ci;

truncate table testing; 
insert into testing (id, uniq_key, normal_col) values
(1, 'uniq1', 'value1'),
(2, 'uniq2', 'value2'),
(3, 'uniq3', 'value3');
SQL;

$app->getDb()->createCommand($sql)->execute();

function print_table(): void
{
    $rows = (new Query())->select('*')->from('testing')->all();
    echo Table::widget([
        'headers' => ['id', 'uniq_key', 'normal_col'],
        'rows' => $rows
    ]);
}

echo 'Before test:' . PHP_EOL;
print_table();

$foundModel = TestingModel::findOne(1);
$foundModel->uniq_key = 'uniq100500';
$foundModel->upsert();

echo 'Model found in DB after upsert: ';
print_r($foundModel->attributes);

$modelLikeDb = new TestingModel([
    'id' => 2,
    'uniq_key' => 'uniq2',
    'normal_col' => 'hello from test'
]);
$ret = $modelLikeDb->upsert();

echo 'New model with ID=2 after upsert returns ' . $ret . ': ';
print_r($modelLikeDb->attributes);

$modelWithUniqConstraint = new TestingModel([
    'uniq_key' => 'uniq3',
    'normal_col' => 'updated to it',
]);
$modelWithUniqConstraint->upsert();

echo 'New model duplicated uniq_key after upsert: ';
print_r($modelWithUniqConstraint->attributes);

$newModel = new TestingModel([
    'uniq_key' => (string)microtime(true),
    'normal_col' => 'some new value'
]);
$newModel->upsert();

echo 'New model without duplicated after upsert: ';
print_r($newModel->attributes);

echo 'After test:' . PHP_EOL;
print_table();