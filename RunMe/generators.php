<?php

/**
 * Calc Fibonacci numbers less or equals N
 *
 * @param int $n
 *
 * @return Generator
 */
function fibonacci($n)
{
    $first = $second = 1;

    while ($first <= $n) {
        yield $first;

        $second = $first + $second;
        $first = $second - $first;
    }
}

foreach (fibonacci(10) as $num) {
    echo "$num ";
}

echo PHP_EOL;