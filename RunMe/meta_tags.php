<?php

use App\TestWebController;

require_once __DIR__ . '/../vendor/autoload.php';

define('YII_DEBUG', true);

$config = [
    'id' => 'test-meta-tags',
    'basePath' => __DIR__ . '/../src',
];
$app = new yii\web\Application($config);

$controller = new TestWebController('test', $app);

$controller->registerMetaTag(['name' => 'description', 'content' => 'test for meta tags registration']);
$controller->registerMetaTag(['name' => 'description', 'content' => 'another description']);

$controller->registerMetaTag(['name' => 'charset', 'charset' => 'windows-1251'], 'charset');
$controller->registerMetaTag(['name' => 'charset', 'charset' => 'utf8'], 'charset');

echo $controller->actionIndex();