<?php

namespace App;

use yii\db\Expression;

/**
 * Trait for Mysql "INSERT ..... ON DUPLICATE KEY UPDATE"
 */
trait BatchTrait
{
    /**
     * Save AR in database if it doesn't exist yet (matching unique constraints) or update changed attributes if it does.
     *
     * @param bool $runValidation
     * @param array $attributeNames
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    public function upsert($runValidation = true, $attributeNames = null)
    {
        if (!$this->getIsNewRecord()) {
            return $this->update($runValidation, $attributeNames);
        }

        if ($runValidation && !$this->validate($attributeNames)) {
            return false;
        }

        return $this->upsertInternal($attributeNames);
    }

    /**
     * @param array $attributes
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    private function upsertInternal(array $attributes = null): bool
    {
        if (!$this->beforeSave(false)) {
            return false;
        }
        $values = $this->getDirtyAttributes($attributes);
        if (empty($values)) {
            $this->afterSave(false, $values);
            return 0;
        }

        $rows = $this->performQuery($values);

        $oldAttributes = $this->getOldAttributes();
        $changedAttributes = [];
        foreach ($values as $name => $value) {
            $changedAttributes[$name] = $oldAttributes[$name] ?? null;
            $oldAttributes[$name] = $value;
        }
        $this->setOldAttributes($oldAttributes);

        $this->afterSave(false, $changedAttributes);

        return $rows > 0;
    }

    /**
     * Perform upsert query
     *
     * @param array $values
     *
     * @return int count of affected rows
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\Exception
     */
    private function performQuery(array $values): int
    {
        $tableName = static::tableName();
        $db = static::getDb();
        $tableSchema = static::getTableSchema();

        $rows = $db->createCommand()->upsert(
          $tableName,
          $values,
          $values
        )->execute();

        $primaryKeys = [];
        foreach ($tableSchema->primaryKey as $name) {
            if ($tableSchema->columns[$name]->autoIncrement) {
                $primaryKeys[$name] =  $db->getLastInsertID($tableSchema->sequenceName);
                break;
            }

            $primaryKeys[$name] = $columns[$name] ?? $tableSchema->columns[$name]->defaultValue;
        }

        foreach ($primaryKeys as $name => $value) {
            $id = static::getTableSchema()->columns[$name]->phpTypecast($value);
            $this->setAttribute($name, $id);
            $values[$name] = $id;
        }

        return $rows;
    }
}