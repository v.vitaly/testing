<?php

namespace App;

use yii\base\Behavior;
use yii\web\Controller;

/**
 * Meta tags registrations for controllers
 */
class ControllerMetaTagsBehavior extends Behavior
{
    /**
     * Register meta tag
     *
     * @param array $options the HTML attributes for the meta tag.
     * @param string $key the key that identifies the meta tag. If two meta tags are registered
     * with the same key, the latter will overwrite the former. If this is null, the new meta tag
     * will be appended to the existing ones.
     *
     * @see \yii\web\View::registerMetaTag
     */
    public function registerMetaTag(array $options, string $key = null)
    {
        /** @var Controller $controller */
        $controller = $this->owner;
        $controller->getView()->registerMetaTag($options, $key);
    }
}