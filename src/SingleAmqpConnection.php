<?php

namespace App;

use PhpAmqpLib\Connection\AbstractConnection;
use RuntimeException;

/**
 * Singleton for AMQP connection
 */
final class SingleAmqpConnection
{
    /**
     * @var SingleAmqpConnection
     */
    private static $instance;

    /**
     * @var AbstractConnection
     */
    private $connection;

    private function __construct()
    {
    }

    /**
     * @return SingleAmqpConnection
     */
    public static function getInstance(): SingleAmqpConnection
    {
        if (self::$instance === null) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    /**
     * @throws RuntimeException
     */
    public function __wakeup()
    {
        throw new RuntimeException('Instance can not be serialized.');
    }

    /**
     * Initialize AMQP connection
     *
     * @param AbstractConnection $connection
     */
    public function initialize(AbstractConnection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return AbstractConnection
     */
    public function getConnection(): AbstractConnection
    {
        return $this->connection;
    }

    private function __clone()
    {
    }
}
