<?php

namespace App;

use yii\web\Controller;

/**
 * Class TestWebController
 */
class TestWebController extends Controller
{
    public $layout = '@app/layout.php';

    /**
     * @return array
     */
    public function behaviors(): array
    {
        return [
          ControllerMetaTagsBehavior::class,
        ];
    }

    /**
     * @return string
     * @throws \yii\base\InvalidArgumentException
     */
    public function actionIndex(): string
    {
        return $this->render('@app/meta.php');
    }
}