<?php

namespace App;

use yii\db\ActiveRecord;

/**
 * Class TestingModel
 *
 * @property int $id
 * @property string $uniq_key
 * @property string $normal_col
 */
class TestingModel extends ActiveRecord
{
    use BatchTrait;

    /**
     * @inheritdoc
     */
    public static function tableName(): string
    {
        return 'testing';
    }
}