<?php

/* @var $this yii\web\View */
/* @var $content string */
?>

<?php $this->beginPage() ?>
  <html lang="en">
  <head>
      <?php $this->head() ?>
  </head>
  <body>
  <?php $this->beginBody() ?>
  <?= $content ?>
  <?php $this->endBody() ?>
  </body>
  </html>
<?php $this->endPage() ?>